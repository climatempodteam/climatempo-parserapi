<?php

namespace ParserApi;

/**
 * Class ConfigAuth
 *
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 * @package ParserApi
 */
class ConfigAuth
{
    /**
     * Server
     */
    const SERVER_TERRA = "http://mterra.climatempo.com.br/api/v1";
    const SERVER_AMAZON = "http://mamazon.climatempo.dev/api/v1";


    /**
     * @var string
     */
    private $server;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param string $server
     * @return ConfigAuth
     */
    public function setServer($server = null)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return ConfigAuth
     */
    public function setUser($user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return ConfigAuth
     */
    public function setPassword($password = null)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function validate()
    {
        if (is_null($this->user)) {
            throw new \Exception("ParserApi ConfigAuth Error: user undefined");
        }

        if (is_null($this->password)) {
            throw new \Exception("ParserApi ConfigAuth error: password undefined");
        }

        if (is_null($this->server)) {
            throw new \Exception("ParserApi ConfigAuth error: server undefined");
        }
    }
}