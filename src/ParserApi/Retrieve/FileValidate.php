<?php

namespace ParserApi\Retrieve;

/**
 * Class File
 *
 *
 * @since
 * @package ParserApi\Retrieve
 * @author  Guilherme Santos <guilhermedossantos91@gmail.com>
 */
class FileValidate extends AbstractParserRetrieve
{

    const STATUS_INVALID = 1;
    const STATUS_VALID = 0;

    /**
     * @param string $directory
     * @param string $file
     * @return null|\stdClass
     * @throws \Exception
     */
    public function getCheckup($directory, $file)
    {
        return $this
            ->setRouter(['file', 'checkup'])
            ->addQueryString("?directory={$directory}&file={$file}")
            ->request();
    }

    /**
     * @param string $directory
     * @param string|null $file
     * @return \stdClass|null
     * @throws \Exception
     */
    public function getValidate($directory, $file = null)
    {
        $queryString = "?directory={$directory}";

        if (!is_null($file)) {
            $queryString .= "&file={$file}";
        }

        return $this
            ->setRouter(['file', 'validate'])
            ->addQueryString($queryString)
            ->request();
    }

    /**
     * @return \stdClass|null
     * @throws \Exception
     */
    public function getDirectories()
    {
        return $this
            ->setRouter(['directories'])
            ->request();
    }

}