<?php

namespace ParserApi\Retrieve;

use ParserApi\ConfigAuth;

/**
 * Class AbstractParserRetrieve
 *
 * @package ParserApi\Retrieve
 */
abstract class AbstractParserRetrieve
{

    /**
     * @var string
     */
    protected $url;

    /**
     * @var ConfigAuth $config
     */
    protected $config;

    /**
     * @var resource
     */
    protected $context = null;

    /**
     * Construct
     *
     * @param ConfigAuth $config
     */
    public function __construct(ConfigAuth $config)
    {
        $this->config = $config;
        $this->config->validate();
        $this->url = $this->config->getServer();
    }

    /**
     * Request authentication config
     *
     * @throws \Exception
     */
    protected function authentication()
    {
        $post = array(
            'user'     => $this->config->getUser(),
            'password' => $this->config->getPassword()
        );

        $http = array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($post)
        );
        $this->context = stream_context_create(array('http' => $http));
    }


    /**
     * API request
     *
     * @return mixed
     * @throws \Exception
     */
    protected function request()
    {
        $this->authentication();
        $content = file_get_contents($this->url, false, $this->context);

        if ($content === false) {
            throw new \Exception("API Error: Invalid server");
        }
        $api = @json_decode($content);

        if (!is_object($api)) {
            throw new \Exception("API Error: Invalid return");
        }

        if (!isset($api->success)) {
            throw new \Exception("API Error: Invalid success");
        }

        if (!isset($api->results)) {
            throw new \Exception("API Error: Invalid results");
        }

        if (!isset($api->message) && !is_null($api->message)) {
            throw new \Exception("API Error: Invalid message");
        }

        if ($api->success == false) {
            throw new \Exception(is_null($api->message) ? "Api Error: undefined error" : $api->message);
        }

        return $api;

    }

    /**
     * Adiciona rota
     *
     * $routers = array(
     *     0 => 'alert',
     *     1 => 'lightning',
     *     2 => 'cep'
     * );
     * http://api.climatempo.com.br/api/v1/alert/lightning/cep
     *
     * @param array $router
     * @return $this
     */
    protected function setRouter(array $router)
    {
        foreach ($router as $value) {
            $this->url .= '/' . $value;
        }

        return $this;
    }

    /**
     * Atribui querystring na rota
     *
     * $queryString = array(
     *     'id' => 1,
     *     'country' => 'brazil'
     * );
     *
     * http://api.climatempo.com.br/api/v1?id=1&country=brazil
     *
     * @param array $queryStrings
     * @return $this
     */
    protected function setQueryString(array $queryStrings)
    {
        $this->url .= http_build_query($queryStrings);
        return $this;
    }

    /**
     * Transforma uma querystring em um array
     *
     * @param string $queryString
     * @param array $params
     * @return array
     */
    protected function queryStringToArray($queryString = null, array &$params)
    {
        $queryString = str_replace("?", "", $queryString);
        parse_str($queryString, $params);
    }

    /**
     * Add querystring na rota
     *
     * @param string $queryString
     * @return $this
     */
    protected function addQueryString($queryString)
    {
        $this->url .= $queryString;
        return $this;
    }

    /**
     * @return string
     */
    protected function getURL()
    {
        return $this->url;
    }

}
